# Avengers Challenge #

La solución fue desarrollada en Python, en un Jupyter Notebook. Es un Storytelling, por lo que falta crear funciones para ordenar un poco más el código. Cualquier duda me pueden contactar.

### Premisa ###

Imagina que eres asesor de Tony Stark y tienes el desafío de ayudarlo a dirigir Stark Industries. Para llevar la empresa al éxito, debes entender bien lo que pasa en ella; juntar data sobre tus clientes para entender de donde vienen, como se comportan y proyectar flujos que te ayuden a tomar decisiones para mantener un buen rumbo y evitar la banca rota.

### Preguntas ###

Los equipos de marketing y contabilidad juntaron algunos datos que creen te podrían ayudar a trabajar estos temas y ahora te toca a partir de ellos resolver los siguientes problemas:

1. ¿Qué recomendaciones le darías a Tony para acumular toda la data de la empresa?
    - Considera data de marketing (canales de adquisición), uso de los productos/servicios, etc.
    - Como harías la data accesible para distintos equipos
    - Que procesos de ETL te imaginas que serían necesarios
    - Tipos de datos relevantes para mostrar a distintas áreas
2. Stark Industries tiene muchos canales de adquisición de clientes (ads en redes sociales, televisión, prensa, SEO, etc) y quisieran saber cuánto aporta cada uno, cómo resolverías lo siguiente: 
    - ¿Cómo definirías el modelo de atribución correcto y por qué?
    - ¿Qué herramientas elegirías para la medición?
    - ¿Cuáles son los errores típicos de trackeo que intentarías evitar desde el comienzo?
3. Proyecta los ingresos y egresos para los próximos 2 meses
    - Todos los datos en la planilla son *dummy*, si algo no te hace sentido, cámbialo a tu criterio argumentando de la mejor manera posible
    - Si te faltan datos, invéntalos

## Todas las respuestas a las preguntas se encuentran dentro del notebook. ##